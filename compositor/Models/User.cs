﻿using System.Collections.Generic;

namespace compositor.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public ICollection<Publish> Publishes { get; set; }
    }
}
