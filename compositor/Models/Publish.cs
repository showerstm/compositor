﻿using System;

namespace compositor.Models
{
    public class Publish
    {
        public Guid Id { get; set; }
        public DateTime Timestamp { get; set; }
        public User User { get; set; }
        public DateTime Expires { get; set; }
        public string Content { get; set; }
    }
}
