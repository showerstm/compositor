﻿using compositor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace compositor.Controllers
{
    [Route("api/users/{username}/[controller]")]
    [ApiController]
    public class PublishesController : ControllerBase
    {
        private readonly UserDb _db;

        public PublishesController(UserDb db)
        {
            _db = db ?? throw new ArgumentNullException(nameof(db));
        }
        
        [HttpGet]
        public ActionResult<IEnumerable<object>> Get([FromRoute] string username)
        {
            var user =
                _db.Users
                .Include(u => u.Publishes)
                .Where(u => u.Username.Equals(username, StringComparison.OrdinalIgnoreCase))
                .FirstOrDefault();

            if (user == null)
                return NotFound();

            var representations = user.Publishes.Select(p => new
            {
                url = Url.Action("Get", "publishes", new { username = p.User.Username, id = p.Id }),
                id = p.Id,
                timestamp = p.Timestamp,
                user = Url.Action("Get", "users", new { username = user.Username }),
                expires = p.Expires,
                content = JObject.Parse(p.Content)
            });

            return Ok(representations);
        }

        [HttpGet("{id}")]
        public ActionResult<object> Get(
            [FromRoute] string username,
            [FromRoute] Guid id)
        {
            var publish = _db.Publishes.Include(p => p.User).FirstOrDefault(p => p.Id == id);

            if (publish == null
                || !publish.User.Username.Equals(username, StringComparison.OrdinalIgnoreCase))
            {
                return NotFound();
            }

            return new
            {
                url = Url.Action("Get", "publishes", new { username = publish.User.Username, id}),
                id,
                timestamp = publish.Timestamp,
                user = Url.Action("Get", "users", new { username = publish.User.Username }),
                expires = publish.Expires,
                content = JObject.Parse(publish.Content)
            };
        }

        [HttpPost]
        public async Task<ActionResult<object>> Post(
            [FromRoute] string username,
            [FromBody] JObject content)
        {
            if (Request.Headers["Expires"].Count != 1)
                return BadRequest();

            if (!DateTime.TryParse(Request.Headers["Expires"].Single(), out DateTime expires))
                return BadRequest();

            var user =
                _db.Users
                .Where(u => u.Username.Equals(username, StringComparison.OrdinalIgnoreCase))
                .FirstOrDefault();

            if (user == null)
                return NotFound();

            var publish = new Publish
            {
                User = user,
                Expires = expires,
                Content = content.ToString(Formatting.None)
            };

            _db.Publishes.Add(publish);
            await _db.SaveChangesAsync();

            var representation = new
            {
                url = Url.Action("Get", "publishes", new { username = publish.User.Username, id = publish.Id}),
                id = publish.Id,
                timestamp = publish.Timestamp,
                user = Url.Action("Get", "users", new { username = publish.User.Username }),
                expires = publish.Expires,
                content = JObject.Parse(publish.Content)
            };

            return CreatedAtAction(nameof(Get), new { username = user.Username }, representation);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<object>> Put(
            [FromRoute] string username,
            [FromRoute] Guid id,
            [FromBody] JObject content)
        {
            if (Request.Headers["Expires"].Count > 1)
                return BadRequest();

            DateTime newExpires = DateTime.MaxValue;
            bool setExpires = Request.Headers["Expires"].Count == 1;

            if (setExpires)
            {
                if (!DateTime.TryParse(Request.Headers["Expires"].Single(), out newExpires))
                    return BadRequest();
            }

            var publish = _db.Publishes.Include(p => p.User).FirstOrDefault(p => p.Id == id);

            if (publish == null
                || !publish.User.Username.Equals(username, StringComparison.OrdinalIgnoreCase))
            {
                return NotFound();
            }

            publish.Content = content.ToString(Formatting.None);
            if (setExpires)
                publish.Expires = newExpires;
            
            await _db.SaveChangesAsync();

            var representation = new
            {
                url = Url.Action("Get", "publishes", new { username = publish.User.Username, id = publish.Id }),
                id = publish.Id,
                timestamp = publish.Timestamp,
                user = Url.Action("Get", "users", new { username = publish.User.Username }),
                expires = publish.Expires,
                content = JObject.Parse(publish.Content)
            };

            return CreatedAtAction(nameof(Get), new { username = publish.User.Username }, representation);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(
            [FromRoute] string username,
            [FromRoute] Guid id)
        {
            var publish = _db.Publishes.Include(p => p.User).FirstOrDefault(p => p.Id == id);

            if (publish == null
                || !publish.User.Username.Equals(username, StringComparison.OrdinalIgnoreCase))
            {
                return NotFound();
            }

            _db.Publishes.Remove(publish);
            await _db.SaveChangesAsync();

            return Ok();
        }
    }
}