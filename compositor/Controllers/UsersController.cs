﻿using compositor.Representations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace compositor.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserDb _db;

        public UsersController(UserDb db)
        {
            _db = db ?? throw new ArgumentNullException(nameof(db));
        }

        [HttpGet]
        public ActionResult<IEnumerable<object>> Get()
        {
            var representations = _db.Users.Select(u => new
            {
                uri = Url.Action("Get", "users", new { username = u.Username }),
                username = u.Username
            }).AsEnumerable();

            return Ok(representations);
        }
        
        [HttpGet("{username}")]
        public ActionResult<string> Get(string username)
        {
            var user =
                _db.Users
                .Include(u => u.Publishes)
                .Where(u => u.Username.Equals(username, StringComparison.OrdinalIgnoreCase))
                .FirstOrDefault();

            if (user == null)
                return NotFound();

            var now = DateTime.Now; // TODO: feel shame

            // TODO: Find a way to have a low-latency, eventually-consistent cache of the
            // latest composite.

            var publishes = user.Publishes.Where(p => p.Expires > now).OrderBy(p => p.Timestamp);

            JObject composite = new JObject();
            foreach (var publish in publishes)
            {
                composite.Merge(JObject.Parse(publish.Content));
            }

            return Ok(new
            {
                uri = Url.Action("Get", "users", new { username = user.Username }),
                username = user.Username,
                state = composite
            });
        }

        [HttpPost]
        public async Task<ActionResult<NewUser>> Post([FromBody] NewUser user)
        {
            _db.Users.Add(new Models.User { Username = user.Username });

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            when (ex.InnerException is SqlException
                && (ex.InnerException as SqlException).Number == 2627)
            {
                return Conflict($"user '{user.Username}' already exists");
            }

            var routeValues = new
            {
                username = user.Username
            };

            var representation = new
            {
                uri = Url.Action("Get", "users", new { username = user.Username }),
                username = user.Username
            };

            return CreatedAtAction(nameof(Get), routeValues, representation);
        }
        
        [HttpDelete("{username}")]
        public async Task<ActionResult> Delete(string username)
        {
            var user =
                _db.Users
                .Where(u => u.Username.Equals(username, StringComparison.OrdinalIgnoreCase))
                .FirstOrDefault();

            if (user == null)
                return NotFound();

            _db.Users.Remove(user);
            await _db.SaveChangesAsync();

            return Ok();
        }
    }
}
