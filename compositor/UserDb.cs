﻿using compositor.Models;
using Microsoft.EntityFrameworkCore;

namespace compositor
{
    public class UserDb : DbContext
    {
        public UserDb(DbContextOptions<UserDb> options)
            : base(options)
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<Publish> Publishes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasAlternateKey(u => u.Username);

            modelBuilder.Entity<Publish>().Property(p => p.Timestamp)
                .ValueGeneratedOnAdd().HasDefaultValueSql("CURRENT_TIMESTAMP()");

            modelBuilder.Entity<Publish>().HasOne(p => p.User).WithMany(u => u.Publishes).IsRequired();
            modelBuilder.Entity<Publish>().Property(p => p.Content).IsRequired();
        }
    }
}
